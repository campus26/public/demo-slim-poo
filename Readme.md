## INIT Projet OO

Dans le cadre du projet vous devez initialiser votre projet en faisant un composer install
ou php bin/composer install si vous n'avez pas COMPOSER d'installé en Global

## Installation

Les librairies suivantes ont été installées pour démarrer le projet :

- slim/slim
- slim/psr7

```
php bin/composer install
```

## Démarrage du projet

Pour démarrer votre projet il suffit d'écrire les commmandes suivantes

```
php -S localhost:9000
```

Attention, l'erreur 404 doit être comprise en fonction du code existant => à vous de comprendre et de corriger
