<?php

namespace Cp26\Dwwm4;

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Main
{
    function __construct()
    {
        $this->initRouter();
    }

    function initRouter()
    {

        // Instantiate App
        $app = AppFactory::create();

        // Add error middleware
        $app->addErrorMiddleware(true, true, true);


        // Add routes
        $app->get('/homepage', function (Request $request, Response $response) {

            $data = [];
            $html = "<h1>Mon code</h1>";
            $response->getBody()->write($html);
            return $response;
        });


        $app->run();
    }
}
